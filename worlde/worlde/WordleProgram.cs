﻿using System;
using System.IO;
using System.Collections.Generic;


//Roger Ribas Sanchez
//DAMV1
namespace WordleRoger
{

    class Wordle
    {
        static void Main()
        {
            var start = new Wordle();
            start.StartingProgram();
        }

        //Archivo ejemplo  Tipo_Idioma_selectedLang.txt        Menu_esp_selectedLang.txt
        // Menu_  Hub_   esp_   selectedLang.txt
        // Title_en_selectedLang.txt
        //Carpeta ejemplo: \Hub || \GetLanguage || \xD

        public void StartingProgram()
        {

            GetLanguage(); //Pedimos un idioma para empezar a ejecutar el juego
            CheckFiles(); //Comprobamos que existan los archivos
            Hub(); //Entramos en el Hub
        }

        //Comprueba que existan los archivos
        public void CheckFiles()
        {
            if (!File.Exists("words.txt"))
            {
                File.Create("words.txt").Close();
                Console.WriteLine("El archivo \\words.txt no se ha podido encontrar");
            }
            if (!File.Exists("wordleInterface.txt"))
            {
                File.Create("wordleInterface.txt").Close();
                Console.WriteLine("El archivo \\wordleInterface.txt no se ha podido encontrar");
            }
            if (!File.Exists("Menu_Hub.txt"))
            {
                File.Create("Menu_Hub.txt").Close();
                Console.WriteLine("El archivo \\Menu_Hub.txt no se ha podido encontrar");
            }
            if (!File.Exists("Hub.txt"))
            {
                File.Create("Hub.txt").Close();
                Console.WriteLine("El archivo \\Hub.txt no se ha podido encontrar");
            }
            if (!File.Exists("Historial.txt"))
            {
                File.Create("Historial.txt").Close();
                Console.WriteLine("El archivo \\Historial.txt no se ha podido encontrar");
            }
            if (!File.Exists("Errors.txt"))
            {
                File.Create("Errors.txt").Close();
                Console.WriteLine("El archivo \\Errors.txt no se ha podido encontrar");
            }
            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }

        //Funcion que muestra un hub con su menú
        public void Hub()
        {
            bool menu = true;
            bool exit = false;


            string name = null;
            do //Bucle para el nombre
            {
                int i = 0; //Contador para mostrar el error si no introduce nada el Usuario

                Console.ForegroundColor = ConsoleColor.Red;
                if (i > 0) Console.WriteLine($"Error. Introduced name is not valid"); //Mensaje de error en caso de que el usuario no introduzca nombre
                Console.ForegroundColor = ConsoleColor.White;

                Console.WriteLine("Player name: ");
                name = Console.ReadLine().Replace(" ", ""); //Pedimos nombre de usuario

                i++;
            } while (name == null);

            do //Bucle de menú
            {
                PrintInterface("Hub.txt", menu); //printamos interfaz del Hub
                char option = Console.ReadKey().KeyChar;
                switch (option)
                {
                    case '1':
                        PlayWordle(RandomWord("words.txt"), "wordleInterface.txt", name); //Extrae de manera aleatoria una palabra del archivo
                        break;
                    case '2':
                        //No entiendo muy bien como hacer para poder volverlo a cambiar, debido a que el current directory ya esta Seteado x'D
                        GetLanguage(); //Permite volver a cambiar el idioma se podria hacer con un SetCurrentDirectory(GetLanguage()) para no ser una funct void
                        break;
                    case '3':
                        Historial(false, 0, " ", " "); //Funcion que muestra el historial de partidas
                        break;
                    case '4':
                        exit = true;
                        break;

                }
            } while (!exit);
        }


        //Funcion que muestra el Historial y en caso de victoria lo registra
        public void Historial(bool victory, int score, string word, string name)
        {
            Console.Clear();
            if (victory) //En caso de victoria registramos la partida
            {
                string record = $@"{name} || {score} || {word}";
                StreamWriter sw;
                sw = File.AppendText("Historial.txt");
                sw.WriteLine("\n " + record);
                sw.Close();
            }

            string[] records = File.ReadAllLines("Historial.txt");
            foreach (string lanes in records) //Mostramos el historial de partidas
            {
                Console.WriteLine(lanes);
            }
            Console.ReadKey();
        }

        //Genera la palabra
        public string RandomWord(string wordsFile)
        {

            string wordsText = File.ReadAllText(wordsFile);
            string[] words = wordsText.Split(',');
            Random rnd = new Random();
            return words[rnd.Next(words.Length)];
        }

        //Funcion que ejecuta el juego mediante otras funciones en su interior
        public void PlayWordle(string word, string file, string name)
        {
            int tries = 7;
            bool victory = false;

            do
            {
                PrintInterface(file, false);

                Console.WriteLine($"\n-{tries}-  |  -WordLength: {word.Length}-");
                //Console.WriteLine(word);

                string usrword = GetUserWord(word, ref tries);

                victory = PrintWord(CheckWord(usrword, word), usrword, 100);

                tries--;
            } while (tries != 0 && !victory);

            Historial(victory, tries, word, name);
        }

        //Obtenemos la palabra del usuario
        public string GetUserWord(string word, ref int tries)
        {
            string userWord = "";

            //Podria haber escrito a mano el error, pero a veces hago estas cosas cuando me olvido momentaneamente de algo,
            //ya que soy olvidadizo a ver si se me queda xD y como aun no se valora optimización me ayuda >:), espero que no sea molestia ^^
            StreamReader reader = new StreamReader("Errors.txt");
            string errorMsg = reader.ReadToEnd();
            string[] error = errorMsg.Split("\n");
            reader.Close();

            do
            {
                Console.WriteLine("______________________________________");
                userWord = Console.ReadLine().ToLower().Replace(" ", "");

                if (userWord.Length != word.Length) { Console.WriteLine(error[1] + " \n Actual word Length = " + word.Length); Console.ReadKey(); Console.Clear(); }
            } while (userWord.Length != word.Length && userWord != "" && userWord != null);

            return userWord;
        }

        //Comprobamos las letras correctas
        public int[] CheckWord(string userWord, string word)
        {

            int[] correctWord = new int[userWord.Length];
            for (int i = 0; i < word.Length; i++)
            {

                correctWord[i] = 0; //La letra no esta en la palabra
                for (int j = 0; j < userWord.Length; j++)
                {
                    if (userWord[i] == word[j])
                    {
                        correctWord[i] = 2; //La letra esta en el lugar equivocado
                    }
                }
                if (userWord[i] == word[i])
                {
                    correctWord[i] = 1; //La letra esta en el lugar correcto
                }
            }
            return correctWord;
        }

        //Printamos por pantalla la palabra
        public bool PrintWord(int[] wordPos, string usrWord, int textspeed)
        {
            int wordCount = 0;
            for (int i = 0; i < wordPos.Length; i++)
            {
                if (wordPos[i] == 1) Console.BackgroundColor = ConsoleColor.Green;
                if (wordPos[i] == 2) Console.BackgroundColor = ConsoleColor.DarkYellow;
                if (wordPos[i] == 0) Console.BackgroundColor = ConsoleColor.Red;
                if (wordPos[i] == 1) wordCount++;

                Console.Write(usrWord[i]);
                Thread.Sleep(textspeed); //Funcion que relentiza la escritura, me hizo gracia y la puse :^)
                Console.BackgroundColor = ConsoleColor.Black;
            }
            Console.ReadKey();

            if (wordCount == wordPos.Length) return true;
            else return false;
        }

        //Obtenemos el idioma en el que e
        public void GetLanguage()
        {

            bool exit = false;
            do
            {
                Console.Clear();
                Console.WriteLine("\tSelect your language");

                Console.WriteLine("\n\t===========================");
                Console.WriteLine("\t     1.-       Català      ");
                Console.WriteLine("\t     2.-       Español     ");
                Console.WriteLine("\t     3.-       English     \n");
                Console.WriteLine("\t===========================");

                char select = Console.ReadKey().KeyChar;
                switch (select)
                {
                    case '1':
                        Directory.SetCurrentDirectory("..\\..\\..\\..\\Wordle\\cat_selectedLang");
                        exit = true;
                        break;

                    case '2':
                        Directory.SetCurrentDirectory("..\\..\\..\\..\\Wordle\\esp_selectedLang");
                        exit = true;
                        break;

                    case '3':
                        Directory.SetCurrentDirectory("..\\..\\..\\..\\Wordle\\en_selectedLang");
                        exit = true;
                        break;

                    default:
                        Console.WriteLine("\n Seleccione un idioma existente");
                        Console.ReadKey();
                        break;
                }
            } while (!exit);
            Console.Clear();
        }


        public void PrintInterface(string file, bool menu)
        {
            Console.Clear();

            //Printamos la interfaz que queramos
            //Añadimos a path el archivo con especific y lang para determinar que queremos printar


            StreamReader getInterface = new StreamReader(file);
            Console.WriteLine(getInterface.ReadToEnd());
            getInterface.Close();

            if (menu) //Si la interfaz es un menú o tiene un menú lo generamos aquí
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                //Cogemos la interfaz del menú
                StreamReader getMenuInterface = new StreamReader("Menu_" + file);
                Console.WriteLine(getMenuInterface.ReadToEnd());
                getInterface.Close();
            }
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
        }
    }
}
