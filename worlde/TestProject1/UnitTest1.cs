using NUnit.Framework;
using WordleRoger;

namespace TestProject1
{
    public class Tests
    {
        //No se muy bien en que usar el test pero aqui he hecho algunos ejemplos que se me han ocurrido
        //seria mas sencillo si hubieran calculos por algun sitio x'D
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void CorrectIntArray()
        {
            Assert.AreEqual(new int[5] { 1, 1, 1, 1, 0 }, WordleRoger.Wordle.CheckWord("pepep", "pepeh"));
        }
        [Test]
        public void TakeWords()
        {
            Assert.AreEqual(true, WordleRoger.Wordle.printWords(new int[4] { 1, 1, 1, 1 }, "paco", 100));
        }
    }
}